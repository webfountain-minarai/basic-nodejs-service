A simple NodeJS server that serves two GET routes & a 404. 

> `GET /`  
`GET /readiness`

Used in Part 1 of an introductory blog series `Building a Web Service with Minikube` (in-progress)

#### To run locally

`npm start`

Navigate to `localhost:3000`

#### To run w/ Docker

Build Image & run container  
`docker build -t basic-nodejs .`  
`docker run -p 3000:3000 -d basic-nodejs`

#### To run w/ Kubernetes

`cd` into the `manifest` directory & apply the current context

`$ kk apply -f .`

#### Resources
* https://github.com/BretFisher/node-docker-good-defaults/blob/master/Dockerfile
* https://github.com/BretFisher/node-docker-good-defaults/blob/69c923bc646bc96003e9ada55d1ec5ca943a1b19/bin/www
* https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js
* https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/
* https://kubernetes.io/docs/concepts/services-networking/service/
